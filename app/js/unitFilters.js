(function () {
    "use strict";

    /* Filters */

    angular.module('unitFilters', []).
//          Length Units
            filter('in2ft', function () {
                return function (inches, rnum) {
                    if (!isNaN(inches)) {
                        var feet = inches / 12;
                        if (!isNaN(rnum)) {
                            return "in = " + feet.toFixed(rnum) + " ft";
                        }
                        return "in = " + feet + " ft";
                    }
                    return inches;
                };
            })
            .filter('in2ftin', function () {
                return function (inches, rnum) {
                    if (!isNaN(inches)) {
                        var feet = inches / 12;
                        var inches2 = inches % 12;
                        if (!isNaN(rnum)) {
                            return "in = " + Math.floor(feet) + " ft " + inches2.toFixed(rnum) + " in";
                        }
                        return "in = " + Math.floor(feet) + " ft " + inches2 + " in";
                    }
                    return inches;
                };
            })
            .filter('ft2in', function () {
                return function (ft, rnum) {
                    if (!isNaN(ft)) {
                        var inches = ft * 12;
                        if (!isNaN(rnum)) {
                            return "ft = " + inches.toFixed(rnum) + " in";
                        }
                        return "ft = " + inches + " in";
                    }
                    return ft;
                };
            })
            .filter('m2ft', function () {
                return function (m, rnum) {
                    if (!isNaN(m)) {
                        var ft = m / (1200 / 3937);
                        if (!isNaN(rnum)) {
                            return "m = " + ft.toFixed(rnum) + " ft";
                        }
                        return "m = " + ft + " ft";
                    }
                    return m;
                };
            })
            .filter('m2ftin', function () {
                return function (m, rnum) {
                    if (!isNaN(m)) {
                        var ft = m / (1200 / 3937);
                        var inches = ((m / (1200 / 3937)) * 12) % 12;
                        if (!isNaN(rnum)) {
                            return "m = " + Math.floor(ft) + " ft " + inches.toFixed(rnum) + " in";
                        }
                        return "m = " + Math.floor(ft) + " ft " + inches + " in";

                    }
                    return m;
                };
            })
            .filter('ft2m', function () {
                return function (ft, rnum) {
                    if (!isNaN(ft)) {
                        var m = ft / (3937 / 1200);
                        if (!isNaN(rnum)) {
                            return "ft = " + m.toFixed(rnum) + " m";
                        }
                        return "ft = " + m + " m";
                    }
                    return ft;
                };
            })
            .filter('in2cm', function () {
                return function (inches, rnum) {
                    if (!isNaN(inches)) {
                        var cm = inches * 2.54;
                        if (!isNaN(rnum)) {
                            return "in = " + cm.toFixed(rnum) + " cm";
                        }
                        return "in = " + cm + " cm";
                    }
                    return inches;
                };
            })
            .filter('cm2in', function () {
                return function (cm, rnum) {
                    if (!isNaN(cm)) {
                        var inches = cm / 2.54;
                        if (!isNaN(rnum)) {
                            return "cm = " + inches.toFixed(rnum) + " in";
                        }
                        return "cm = " + inches + " in";
                    }
                    return cm;
                };

            })

//          Speed Units
            .filter('mph2knots', function () {
                return function (mph, rnum) {
                    if (!isNaN(mph)) {
                        var knots = mph / 1.15077945;
                        if (!isNaN(rnum)) {
                            return "mph = " + knots.toFixed(rnum) + " knots";
                        }
                        return "mph = " + knots + " knots";
                    }
                    return mph;
                };
            })
            .filter('knots2mph', function () {
                return function (knots, rnum) {
                    if (!isNaN(knots)) {
                        var mph = knots * 1.15077945;
                        if (!isNaN(rnum)) {
                            return "knots = " + mph.toFixed(rnum) + " mph";
                        }
                        return "knots = " + mph + " mph";
                    }
                    return knots;
                };
            })
            .filter('mph2kph', function () {
                return function (mph, rnum) {
                    if (!isNaN(mph)) {
                        var kph = mph * 1.609344;
                        if (!isNaN(rnum)) {
                            return "mph = " + kph.toFixed(rnum) + " kph";
                        }
                        return "mph = " + kph + " kph";
                    }
                    return mph;
                };
            })
            .filter('kph2mph', function () {
                return function (kph, rnum) {
                    if (!isNaN(kph)) {
                        var mph = kph / 1.609344;
                        if (!isNaN(rnum)) {
                            return "kph = " + mph.toFixed(rnum) + " mph";
                        }
                        return "kph = " + mph + " mph";
                    }
                    return kph;
                };
            })

//          Pressure Units
            .filter('psi2kpa', function () {
                return function (psi, rnum) {
                    if (!isNaN(psi)) {
                        var kpa = psi / 0.14503773773020923;
                        if (!isNaN(rnum)) {
                            return "psi = " + kpa.toFixed(rnum) + " kPa";
                        }
                        return "psi = " + kpa + " kPa";
                    }
                    return psi;
                };
            })
            .filter('kpa2psi', function () {
                return function (kpa, rnum) {
                    if (!isNaN(kpa)) {
                        var psi = kpa / 6.894757293168361;
                        if (!isNaN(rnum)) {
                            return "kPa = " + psi.toFixed(rnum) + " psi";
                        }
                        return "kPa = " + psi + " psi";
                    }
                    return kpa;
                };
            })

//            Weight Units
            .filter('lbs2kg', function () {
                return function (lbs, rnum) {
                    if (!isNaN(lbs)) {
                        var kg = lbs / 2.20462;
                        if (!isNaN(rnum)) {
                            return "lbs = " + kg.toFixed(rnum) + " kg";
                        }
                        return "lbs = " + kg + " kg";
                    }
                    return lbs;
                };
            })
            .filter('kg2lbs', function () {
                return function (kg, rnum) {
                    if (!isNaN(kg)) {
                        var lbs = kg * 2.20462;
                        if (!isNaN(rnum)) {
                            return "kg = " + lbs.toFixed(rnum) + " lbs";
                        }
                        return "kg = " + lbs + " lbs";
                    }
                    return kg;
                };
            })

//          Temperature Units
            .filter('f2c', function () {
                return function (f, rnum) {
                    if (!isNaN(f)) {
                        var c = (f - 32) * 5 / 9;
                        if (!isNaN(rnum)) {
                            return "\u2109 = " + c.toFixed(rnum) + " \u2103";
                        }
                        return "\u2109 = " + c + " \u2103";
                    }
                    return f;
                };
            })
            .filter('c2f', function () {
                return function (c, rnum) {
                    if (!isNaN(c)) {
                        var f = c * 9 / 5 + 32;
                        if (!isNaN(rnum)) {
                            return "\u2103 = " + f.toFixed(rnum) + " \u2109";
                        }
                        return "\u2103 = " + f + " \u2109";
                    }
                    return c;
                };
            })
            .filter('c2k', function () {
                return function (c, rnum) {
                    if (!isNaN(c)) {
                        var k = Math.abs(c) + 273.15;
                        if (!isNaN(rnum)) {
                            return "\u2103 = " + k.toFixed(rnum) + " K";
                        }
                        return "\u2103 = " + k + " K";
                    }
                    return c;
                };
            })
            .filter('k2c', function () {
                return function (k, rnum) {
                    if (!isNaN(k)) {
                        var c = Math.abs(k) - 273.15;
                        if (!isNaN(rnum)) {
                            return "K = " + c.toFixed(rnum) + " \u2103";
                        }
                        return "K = " + c + " \u2103";
                    }
                    return k;
                };
            });
}());